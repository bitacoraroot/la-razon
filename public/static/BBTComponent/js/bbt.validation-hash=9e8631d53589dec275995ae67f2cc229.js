Namespace.Register("BBT.validation");

BBT.validation.validate = function (jObj) {

	if (jObj == null) {
		jObj = jQuery(document);
	}
	
	// global errors
	var errors = {};
	jObj.find(".bb-field.bb-field-validation").each(function (__i, data) {			
			var name = jQuery(data).attr("name");
			var _errors = BBT.validation.validateField(jQuery(data));
			if(_errors.length > 0) {
				errors[name] = _errors;
			}
	});

	return errors;
};

BBT.validation.isValidField = function (jObj) {
	var _errors = BBT.validation.validateField(jObj);
	if(_errors.length > 0) {
		return false;
	}
	return true;
};

BBT.validation.validateField = function (jObj) {

	       var name = jObj.attr("name");
	       var value = BBT.getFieldValue(name, jObj.parent(), true);

	       var _classes = jObj.attr("class").split(" ");
	       var _functions = [];
	       var _regexp = new RegExp("^bb-field-validation_");
		       
	       var isRequired = false;
	       jQuery.each(_classes, function(__j, fieldClass) {  
				       if(fieldClass.match(_regexp)) {
					       var fieldFunction = fieldClass.replace(_regexp, ""); 
					       if(fieldFunction == "required") isRequired = true;
					       _functions[_functions.length] = fieldFunction;
				       }
		       });	

	       try {
		       // current field errors
		       var _errors = [];
		       jQuery.each(_functions, function (__x, vfunction) {
				       var ret = BBT.validation.functions[vfunction](value);
				       if(ret == false) {
					       // ignore errors if not required
					       if(isRequired) {
						       _errors[_errors.length] = vfunction;
					       }
				       }
		       });
		       return _errors;
	       } catch(e) {
		       BBT.log(e);
		       // not defined
	       }	       

};


// Validate functions

BBT.validation.functions = {};

BBT.validation.functions["required"] = function (value) {
	if(value == "") {
		return false;
	} 
	return true;
};

BBT.validation.functions["email"] = function (value) {
	var filter = /^([a-zA-Z0-9_.-])+@(([a-zA-Z0-9-])+.)+([a-zA-Z0-9]{2,4})+$/;
	if (!filter.test(value)) {
		return false;
	} 
	return true;
};

// value = array(accountEnt, accountSc, accountDC, accountNum)
BBT.validation.functions["ccc"] = function (value) {

	// TODO: check values
	var accountEnt = value[0];
	var accountSc = value[1];
	var accountDC = value[2];
	var accountNum = value[3];

	function obtainDC(valor){
		valores = new Array(1, 2, 4, 8, 5, 10, 9, 7, 3, 6);
		control = 0;
		for (i=0; i<=9; i++) {
			control += parseInt(valor.charAt(i)) * valores[i];
		}
		control = 11 - (control % 11);
 
		if (control == 11) control = 0;
		else if (control == 10) control = 1;
 
		return control;
	}


	if (!(obtainDC("00" + parseInt(accountEnt,10) + parseInt(accountSc,10)) == parseInt(accountDC.charAt(0))) || !(obtainDC(accountNum) == parseInt(accountDC.charAt(1))))  {
		return false;
	} 
	return true;
};


