// Copyright 2011 Bitban Technologies, S.L.
// Todos los derechos reservados.
//LaRazon JS Functions

Namespace.Register("LRZRazon");

LRZRazon.contact = function (jObj) {

        jObj.ajaxForm({
	    beforeSerialize: function($form, options) {
				var jform = jQuery($form);
				var errors = BBT.validation.validate(jObj);

				var isValid = true;
				var msgError = "Todos los campos del formulario son obligatorios. Por favor, vuelva a intentarlo";
				jQuery.each(errors, function (field, _errors) {
 						if (field == "contactTerms") {
 							msgError = "Debe introducir aceptar los términos y condiciones para poder enviar el mensaje";
 						}
 						if (field == "contactOptions") {
 							msgError = "Debe seleccionar algún destinatario del selector superior para poder enviar el mensaje";
 						}
						if (field == "contactMail") {
 							msgError = "El email proporcionado es erroneo";
 						}
						isValid = false;
					});
				if (isValid == false) {
					LRZRazon.show_message(msgError, jObj);
					LRZRazon.new_captcha(jObj);
					jQuery("#bb-input-captcha").val("");
					return false;
				}
			},
	    success: function(_data) {
 				var data = BBT.fixJson(_data);
 				if (data["error"] == 0) {
					jObj.find(".confirmMsg").show();
 				} else {
					LRZRazon.show_message(data["message"], jObj);
					LRZRazon.new_captcha(jObj);
					jQuery("#bb-input-captcha").val("");
 				}
			}
		});
        jObj.submit();
	jQuery('[title!=""]').hint();
	return false;
};

LRZRazon.show_message = function (msg, jObj) {
	box = jObj.find(".errorMsg");
	box.empty();
	box.append("<p>" + msg + "</p>");
	box.show();
};
LRZRazon.new_captcha = function (jObj) {
	var _image = new Image();
	var imageurl = "/bbtcaptcha/captcha?config=contactCaptchaConfig&rand=" + Math.round(Math.random() * 1000);
	_image.src = imageurl;
	jObj.find(".captcha-img").empty();
	jObj.find(".captcha-img").append(_image);
};

BBT.validation.functions["check"] = function (value) {
	if (value == "checked") {
		return true;
	}
	
	return false;
};

BBT.validation.functions["option"] = function (value) {
	if (value != "") {
		return true;
	}
	
	return false;
};